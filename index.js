const  sum = (a,b) => {
  return a + b
}

function* z(a,b) {
  let res = yield (x = a,y = b) => sum(x,y); // 3
  console.log("res 1: " + res);
  res = yield 3+3; // 6
  console.log("res 2: " + res);
  res = yield sum(res,res); //12
  console.log("finish : " + res) //12
  return res;
}

function worker(gen) {
  const test = gen(1,2);
  let elem = test.next();
  while (elem.done === false) {
    if (typeof elem.value === "function") {
      elem = test.next(elem.value());
    }
    else {
      elem = test.next(elem.value);
    }
  }
  return elem.value
}

const res = worker(z)
console.log(res);